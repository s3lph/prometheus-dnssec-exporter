
PDE_VERSION=$$(git describe --tags --dirty | sed -re s/^v// | cut -d- -f1)
PDE_REVISION=$$(git describe --tags --dirty | cut -d- -f2-)

LD_VERSION=-X 'github.com/prometheus/common/version.Version=$(PDE_VERSION)'
LD_REVISION=-X 'github.com/prometheus/common/version.Revision=$(PDE_REVISION)'
LD_BRANCH=-X 'github.com/prometheus/common/version.Branch=$(CI_COMMIT_REF_NAME)'
LD_USER=-X 'github.com/prometheus/common/version.BuildUser=$$(whoami)@$$(hostname -f)'
LD_DATE=-X 'github.com/prometheus/common/version.BuildDate=$$(date --iso-8601=seconds)'
LDFLAGS=-s $(LD_VERSION) $(LD_REVISION) $(LD_BRANCH) $(LD_USER) $(LD_DATE)

prometheus-dnssec-exporter:
	go build -tags netgo -ldflags="$(LDFLAGS)" -o prometheus-dnssec-exporter
	./prometheus-dnssec-exporter --version

.changes:
	git log --format='- %s' $$(git describe --tags --abbrev=0 @^)..@ > .changes

clean:
	rm -f prometheus-dnssec-exporter .changes
